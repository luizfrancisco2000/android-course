package com.luiz.course.udacity.diceroll

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.luiz.course.udacity.diceroll.databinding.ActivityMainBinding
import java.util.*


class MainActivity : AppCompatActivity() {

    // var diceImage: ImageView
    private lateinit var binding: ActivityMainBinding

    private val myName: MyName = MyName("Luiz francisco")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.myName = myName
        //findViewById<Button>(R.id.done_button).setOnClickListener {
       //     addNickName(it)
        //}
        binding.doneButton.setOnClickListener {
            addNickName(it)
        }
    }

    private fun addNickName(view: View) {
     //   val editText = findViewById<EditText>(R.id.nickname_edit)
      //  val nicknameTextView = findViewById<TextView>(R.id.nickname_text)
        binding.apply {
            //binding.nicknameText.text = binding.nicknameEdit.text
            myName?.nickaname = binding.nicknameEdit.text.toString()
            invalidateAll()
            binding.nicknameEdit.visibility = View.GONE
            binding.doneButton.visibility = View.GONE
            binding.nicknameText.visibility = View.VISIBLE
        }

        // Hide the keyboar
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}